import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'model.dart';



void main() => runApp(
  ChangeNotifierProvider(
    builder: (contex) => DataModel(),
    child: MyApp()
  )
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      home: Menu(),
    );
  }
}

class Menu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          centerTitle: true,
          title: Text("Pingo"),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Color(0xFFDDDDDD),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                
                Padding(
                  padding: EdgeInsets.fromLTRB(0,0,0,50),
                  child: GestureDetector(
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(50)),
                      child: Image.asset(
                        Provider.of<DataModel>(context).iconName(),
                        height:100,
                        width:100
                        ),
                      ),
                    onTap: () {
                      print(Provider.of<DataModel>(context).iconName());
                      Provider.of<DataModel>(context).nextPerson();
                    },
                  ),
                ),
                RaisedButton(
                  child: Text('3x3'),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Game()),
                    );
                  },
                ),
                RaisedButton(
                  child: Text('Témata'),
                  onPressed: () {
                    Provider.of<DataModel>(context).loadFileTopics().then((arg){
                      Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Themes()),
                      );
                    });
                  },
                ),
                
              ]
            ),
          ),
        ),
      );
  }
}

class Game extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          centerTitle: true,
          title: Text("Pingo 3x3"),
        ),
        body: Container(
          decoration: BoxDecoration(
            color: Color(0xFFDDDDDD),
          ),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                BingoSign(),
                Field(),
                ResetButton(),
                ]
                ,),
              Provider.of<DataModel>(context).pospec?Positioned(
                
                width: MediaQuery.of(context).size.width,
                top: 150,
                child: Image.asset('assets/pospec.png')
              ):Text(""),
              Provider.of<DataModel>(context).pospec?Positioned(
                width: MediaQuery.of(context).size.width,
                child: GestureDetector(
                  onTap: Provider.of<DataModel>(context).resetPospec,
                  child: Image.asset('assets/confetti.gif')
                  )
              ):Text(""),
            ],
          )
          ),
      );
  }
}

class Field extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    List<Card> cards = [];
    for (var i = 0; i < 9; i++) {
      cards.add(new Card(id: i));
    }

    return GridView.count(
          crossAxisCount: 3,
          childAspectRatio: 1.0,
          padding: const EdgeInsets.all(10),
          mainAxisSpacing: 10,
          crossAxisSpacing: 10,
          children: cards,
          shrinkWrap: true,
        );
  }
}

class Card extends StatefulWidget{
  Card({this.id});

  final int id;

  @override
  _CardState createState() => _CardState();
}

class _CardState extends State<Card> {

  void _press(){
    final data = Provider.of<DataModel>(context);
    data.press(widget.id);
  }

  @override
  Widget build(BuildContext context){
    return Consumer<DataModel>(
          builder: (context, data, child) => GestureDetector(
            onTap: _press,
            child: Container(
          decoration: new BoxDecoration(
            boxShadow: [
              new BoxShadow(
                color: Colors.black,
                blurRadius: 4.0,
                offset: Offset(0, 2)
              ),
            ],
            color: data.cards[widget.id]? Colors.green : Colors.grey,
            borderRadius: new BorderRadius.all(new Radius.circular(20))
          ),
          child:Center(child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(data.labels[widget.id], style: TextStyle(color: Colors.white, fontSize: 11,), textAlign: TextAlign.center,),
          )),
          
        ),
      )
    );
  }
}

class ResetButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text("Nová hra"), 
      color: Colors.grey,
      onPressed: Provider.of<DataModel>(context).reset
      );
  }
}

class BingoSign extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0),
      child: Container(
        child: 
          Consumer<DataModel>(
            builder: (context, data, child) => Text(
              data.won? "Bingo!":"",
              style: TextStyle(fontSize: 50, color: Colors.green)
              ),
            )
      ),
    );
  }
}

class Themes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle:true,
        title: Text("Editor témat"),
      ),
      body: Center(
        child: ThemeList()
        )
    );
  }
}


class ThemeList extends StatelessWidget {
  
  Widget  _item(BuildContext context, String label){
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 2.0, color: Colors.grey)
        )
      ),
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(label.toUpperCase(), style: TextStyle()),
            GestureDetector(
              onTap: () => Provider.of<DataModel>(context).removeTopic(label),
              child: Icon(
                Icons.delete,
                color: Color(0xffb81414),
                size: 30.0,
              ),
            ),
          ],
        )
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    return Padding(
      padding: EdgeInsets.all(10),
      child: Consumer<DataModel>(
        builder: (context, data, child) => ListView(
          children: List.from(data.topics.map((label) => _item(context, label)).toList())..addAll([Padding(
            padding: EdgeInsets.fromLTRB(0,30,0,30),
            child: TextField(
              onSubmitted: (text) => Provider.of<DataModel>(context).addTopic(text),
              decoration: InputDecoration(
                hintText: "NOVÉ TÉMA",
              ),
              ),
          )]),
        )
      ),
    );
  }
}