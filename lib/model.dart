import 'package:flutter/foundation.dart';
import 'dart:math';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';

class DataModel extends ChangeNotifier{
  DataModel(){
    this.loadFileTopics();
  }
  
  List<String> people = ["pospec"/*,"necas"*/,"kobza","boucnik"];
  int currentPerson = 0;

  Map originalTopics = {
    "pospec":["filmy","budoucnost studenta","politika","střední východ","(ne)známé osoby","jeho kamarádi/známí","hudba","\"já jsem upřímný\"","politická korektnost","mrkosovninky (matematika)","kontrolní otázka","bulvární časopisy","jůtůbeři","\"jak jsem se k tomu dostal?\"","proroctví pro čr","melancholik","\"no to víš že jo\"","jaroška behind the scenes","\"já už na to kašlu...\"","online","evropská unie","docházka studentů","povídání si s oblíbeným studentem","koloběžkáři","\"nechce se mi zkoušet\"","náboženství","trump(eta)","literární autoři","socialismus","kecy v kleci","soudy a právo", "Brexit", "Genderová vyváženost", "MIT"],
    /*"necas":["Omluvenky", "Černá listina", "Proč to nefunguje?", "Tohle jsem ukradl",],*/
    "kobza":["Jáj","Ženuška","Nelinka","Matematika je co?","Před použitím protřepat","Písničky","Duchovní potrava","Sprostě se nemluví","Nákupy", "Sportka", "Last but not least", "Máme radost", "Týdeňáček", "Mirek dušín", "Nekrademe", "Domácí úloha", "Zívačka", "Barvičky", "Moje nervy", "Dáma v letech", "Sním, či bdím?", "Zvoláme spor", "Nezáporné", "Hezký obrázek napoví ...", "Brambory", "Za vším hledej kružnici", "Reality show", "DJ Šmudla", "Nejsme troškaři", "Máme službu?", "Co tím chtěl básník říct?", "Důvěrně známý vzorec", "Krotký jako beránek", "Tím neoslním", "Casio", "Alfréd a Blažena", "No comment"],
    "boucnik":["Šílený", "Malinô Brdo", "Takže jedem", "Strašně důležitý", "Děcka", "Neskutečný", "Nestíháme", "Příště budu zkoušet", "Ještě ty dvě pluska", "Přetáhneme do přestávky", "Na řádek", "S vykřičníkem",  "Myslím, že je to jasné", "Kdo tohle pochopil?"],
  };

  String iconName(){
    
    return "assets/"+people[currentPerson]+"_icon.jpg";
  }

  String _fileName(){
    return people[currentPerson]+".txt";
  }
  List<String> topics = [];

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    String fileName = _fileName();
    return File('$path/$fileName.txt');
  }

  Future<File> writeFile(String data) async {
    final file = await _localFile;
    return file.writeAsString(data);
  }

  Future<String> readFile() async {
    try {
      final file = await _localFile;
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      return "";
    }
  }

  Future loadFileTopics() async{
    String fileContents = await readFile();
    if(fileContents == ""){
      await writeFile(originalTopics[people[currentPerson]].join("\r\n"));
    }
    else{
      topics = fileContents.split("\r\n");
    }
  }

  Future addTopic(String topic) async{
    topics.add(topic);
    await writeFile(topics.join("\r\n"));
    await loadFileTopics();
    notifyListeners();
  }
  
  Future removeTopic(String topic) async{
    topics.remove(topic);
    await writeFile(topics.join("\r\n"));
    await loadFileTopics();
    notifyListeners();
  }

  void nextPerson(){
    print("NextPerson");
    currentPerson += 1;
    if(currentPerson > (people.length-1))
      currentPerson = 0;
    notifyListeners();
  }



  List<bool> cards = List<bool>.filled(9, false);
  List<String> labels = List<String>.filled(9, "");
  
  bool won = false;
  bool pospec = false;

  void checkForThreeInRow(){
    List<List<int>> winCombinations = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
    for(List<int> combination in winCombinations){
      if(cards[combination[0]] & cards[combination[1]] & cards[combination[2]]){
        won = true;
        pospec = true;
      }
    }
  }

  void press(int id){
    cards[id] = !cards[id];
    checkForThreeInRow();
    notifyListeners();
  }

  void reset(){
    print("lolec");
    loadFileTopics().then((arg){
    cards = [false, false, false, false, false, false, false, false, false];
    List<int> freeIndexes = [];
    for(var i = 0; i<topics.length; i++){
      freeIndexes.add(i);
    }
    print(freeIndexes);
    for (var i = 0; i < 9; i++) {
      if(freeIndexes.length > 0){
        int rnd =  Random().nextInt(freeIndexes.length);
        labels[i] = topics[freeIndexes[rnd]].toUpperCase();
        freeIndexes.remove(freeIndexes[rnd]);
      }
      else
        labels[i] = "NEDOSTATEK KARET";
    }
    won = false;
    pospec = false;
    notifyListeners();
    });
  }

  void resetPospec(){
    pospec = false;
    notifyListeners();
  }
}
